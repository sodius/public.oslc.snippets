package com.sodius.oslc.snippets.utils;

import java.text.Collator;
import java.util.Comparator;

import org.eclipse.lyo.oslc4j.core.model.IExtendedResource;

import com.sodius.oslc.core.model.Dcterms;
import com.sodius.oslc.core.model.ResourceProperties;

/**
 * Compares two resources by title
 */
public class TitleComparator implements Comparator<IExtendedResource> {

    @Override
    public int compare(IExtendedResource r1, IExtendedResource r2) {
        // Use ResourceProperties to get the dcterms:title property,
        // as we may have various subclasses of IExtendedResource which define their own getTitle() method
        String title1 = ResourceProperties.getString(r1, Dcterms.PROPERTY_TITLE);
        String title2 = ResourceProperties.getString(r2, Dcterms.PROPERTY_TITLE);

        return Collator.getInstance().compare(title1, title2);
    }
}