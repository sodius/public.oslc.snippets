package com.sodius.oslc.snippets.utils;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.lyo.oslc4j.core.model.IExtendedResource;

import com.sodius.oslc.core.model.Foaf;
import com.sodius.oslc.core.model.Person;
import com.sodius.oslc.core.process.model.LinkType;
import com.sodius.oslc.domain.am.model.ArchitectureResource;
import com.sodius.oslc.domain.am.model.OslcAm;
import com.sodius.oslc.domain.cm.model.ChangeRequest;
import com.sodius.oslc.domain.cm.model.OslcCm;
import com.sodius.oslc.domain.config.model.ChangeSet;
import com.sodius.oslc.domain.config.model.OslcConfig;
import com.sodius.oslc.domain.qm.model.OslcQm;
import com.sodius.oslc.domain.qm.model.TestCase;
import com.sodius.oslc.domain.qm.model.TestExecutionRecord;
import com.sodius.oslc.domain.qm.model.TestPlan;
import com.sodius.oslc.domain.qm.model.TestResult;
import com.sodius.oslc.domain.qm.model.TestScript;
import com.sodius.oslc.domain.rm.model.OslcRm;
import com.sodius.oslc.domain.rm.model.Requirement;
import com.sodius.oslc.domain.rm.model.RequirementCollection;

/**
 * Utilities on link types.
 */
public class LinkTypeUtils {

    private static final Map<String, Class<? extends IExtendedResource>> TYPES = new HashMap<>();

    static {
        // FOAF
        TYPES.put(Foaf.TYPE_PERSON, Person.class);

        // OSLC AM
        TYPES.put(OslcAm.TYPE_AMRESOURCE, ArchitectureResource.class);

        // OSLC CM
        TYPES.put(OslcCm.TYPE_CHANGE_REQUEST, ChangeRequest.class);

        // OSLC QM
        TYPES.put(OslcQm.TYPE_TEST_CASE, TestCase.class);
        TYPES.put(OslcQm.TYPE_TEST_EXECUTION_RECORD, TestExecutionRecord.class);
        TYPES.put(OslcQm.TYPE_TEST_PLAN, TestPlan.class);
        TYPES.put(OslcQm.TYPE_TEST_RESULT, TestResult.class);
        TYPES.put(OslcQm.TYPE_TEST_SCRIPT, TestScript.class);

        // OSLC RM
        TYPES.put(OslcRm.TYPE_REQUIREMENT, Requirement.class);
        TYPES.put(OslcRm.TYPE_REQUIREMENT_COLLECTION, RequirementCollection.class);

        // OSLC Config
        TYPES.put(OslcConfig.TYPE_CHANGE_SET, ChangeSet.class);
        TYPES.put("http://open-services.net/ns/rm#ChangeSet", ChangeSet.class);
    }

    /**
     * Returns the English label for a link type (e.g. "Related Change Request").
     */
    public static String getTitle(LinkType linkType) {
        return linkType.getTitle(Collections.emptyList() /* use default locale */);
    }

    /**
     * Returns the Java class (e.g. ChangeRequest) to use for loading the source of a link type.
     */
    public static Class<? extends IExtendedResource> getSourceType(LinkType linkType) {
        return getType(linkType.getSourceRange());
    }

    /**
     * Returns the Java class (e.g. Requirement) to use for loading the target of a link type.
     */
    public static Class<? extends IExtendedResource> getTargetType(LinkType linkType) {
        return getType(linkType.getTargetRange());
    }

    /**
     * Returns the Java class (e.g. Requirement) to use for loading the specified type.
     */
    public static Class<? extends IExtendedResource> getType(URI range) {
        Class<? extends IExtendedResource> targetType = TYPES.get(range.toString());
        if (targetType == null) {
            throw new RuntimeException("Unsupported target range: " + range);
        } else {
            return targetType;
        }
    }

    private LinkTypeUtils() {
    }

}
