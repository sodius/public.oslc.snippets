package com.sodius.oslc.snippets.app.jazz;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.eclipse.lyo.oslc4j.core.model.ServiceProviderCatalog;

import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetRootServices;
import com.sodius.oslc.client.requests.GetServiceProviderCatalog;
import com.sodius.oslc.core.model.RootServices;
import com.sodius.oslc.snippets.utils.TitleComparator;

/**
 * Validates a given Root Services location and loads the declared catalog.
 * It notably ensures the server is accessible and the provided credentials allows connecting to the server.
 * It then reads the Root Services document to determine the location of the Service Provider Catalogs.
 * It finally loads the declared catalog to list the available projects.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jazz.user</code> - user ID to connect to Jazz.</li>
 * <li><code>jazz.password</code> - password of the specified Jazz user.</li>
 * <li><code>jazz.rootservices</code> - URL of the Jazz application Root Services
 * (e.g. <code>https://myserver.mydomain.com:9443/rm/rootservices</code></li>
 * </ul>
 */
public class ListProjectsSnippet extends JazzSnippet {

    public static void main(String[] args) throws Exception {
        new ListProjectsSnippet().call();
    }

    @Override
    protected void run(OslcClient client) {

        // read root services
        System.out.println("Reading Root Services...");
        URI rootServicesLocation = URI.create(getRequiredProperty("jazz.rootservices"));
        RootServices rootServices = new GetRootServices<>(rootServicesLocation, RootServices.class).call();
        System.out.println();

        // read catalogs
        readCatalog(client, rootServices.getCmCatalog(), "Change Management");
        readCatalog(client, rootServices.getRmCatalog(), "Requirements Management");
        readCatalog(client, rootServices.getQmCatalog(), "Quality Management");
        readCatalog(client, rootServices.getAmCatalog(), "Architecture Management");
    }

    private void readCatalog(OslcClient client, URI catalogLocation, String domainDescriptor) {

        // root services might not declare this type of catalog
        if (catalogLocation == null) {
            System.out.println(domainDescriptor + " Catalog: not defined in Root Services document");
            System.out.println();
        }

        else {
            System.out.println("Reading " + domainDescriptor + " Catalog...");
            ServiceProviderCatalog catalog = new GetServiceProviderCatalog(client, catalogLocation).get();

            // sort projects by title
            List<ServiceProvider> providers = new ArrayList<>(Arrays.asList(catalog.getServiceProviders()));
            Collections.sort(providers, new TitleComparator());

            // print out the projects
            System.out.println(domainDescriptor + " Catalog has " + providers.size() + " projects:");
            for (ServiceProvider provider : providers) {
                System.out.println(" - " + provider.getTitle() + ": <" + provider.getAbout() + '>');
            }
            System.out.println();
        }
    }

}
