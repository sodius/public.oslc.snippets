package com.sodius.oslc.snippets.app.jazz;

import java.net.URI;

import org.eclipse.lyo.oslc4j.core.model.Publisher;

import com.sodius.oslc.app.jazz.model.JazzFoundation;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetPublisher;
import com.sodius.oslc.client.requests.GetRootServices;
import com.sodius.oslc.core.model.ResourceProperties;
import com.sodius.oslc.core.model.RootServices;

/**
 * Validates a given Root Services location and loads the Publisher.
 * It notably ensures the server is accessible and the provided credentials allows connecting to the server.
 * It then reads the Root Services document to determine the location of the Publisher.
 * It finally loads the Publisher to determine the application's information.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jazz.user</code> - user ID to connect to Jazz.</li>
 * <li><code>jazz.password</code> - password of the specified Jazz user.</li>
 * <li><code>jazz.rootservices</code> - URL of the Jazz application Root Services
 * (e.g. <code>https://myserver.mydomain.com:9443/rm/rootservices</code></li>
 * </ul>
 */
public class ReadApplicationVersion extends JazzSnippet {

    public static void main(String[] args) throws Exception {
        new ReadApplicationVersion().call();
    }

    @Override
    protected void run(OslcClient client) {

        // read root services
        System.out.println("Reading Root Services...");
        URI rootServicesLocation = URI.create(getRequiredProperty("jazz.rootservices"));
        RootServices rootServices = new GetRootServices<>(rootServicesLocation, RootServices.class).call();
        System.out.println();

        // read publisher
        System.out.println("Reading publisher...");
        Publisher publisher = new GetPublisher(rootServices.getPublisher()).get();
        System.out.println("Title: " + publisher.getTitle());
        System.out.println("Identifier: " + publisher.getIdentifier());
        System.out.println("Version: " + ResourceProperties.getString(publisher, JazzFoundation.PROPERTY_VERSION));
    }

}
