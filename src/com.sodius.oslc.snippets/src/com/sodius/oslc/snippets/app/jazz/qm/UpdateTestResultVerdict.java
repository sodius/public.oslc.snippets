package com.sodius.oslc.snippets.app.jazz.qm;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.eclipse.lyo.oslc4j.core.model.IExtendedResource;
import org.eclipse.lyo.oslc4j.core.model.Property;
import org.eclipse.lyo.oslc4j.core.model.ResourceShape;

import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetResource;
import com.sodius.oslc.client.requests.GetResourceShape;
import com.sodius.oslc.client.requests.PutResource;
import com.sodius.oslc.client.requests.ResourceResponse;
import com.sodius.oslc.core.model.Dcterms;
import com.sodius.oslc.core.model.ResourceProperties;
import com.sodius.oslc.domain.qm.model.TestResult;
import com.sodius.oslc.snippets.app.jazz.JazzSnippet;

/**
 * Updates the verdict of the specified test result.
 * A list of available verdicts is displayed in the console and user has to change the one to apply on the test result.
 * </p>
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jazz.user</code> - user ID to connect to Jazz.</li>
 * <li><code>jazz.password</code> - password of the specified Jazz user.</li>
 * <li><code>jazz.artifact</code> - URL of the ETM test result.</li>
 * </ul>
 *
 * <h3>Test Result URL</h3>
 * <p>
 * The URL of the test result is the OSLC URL of the artifact, which you can obtain from /qm (versions 6.0.6.1 and higher):
 * <ol>
 * <li>Navigate to the test artifact page.</li>
 * <li>Click the "Copy link for this page" button in the tool bar</li>
 * <li>Click "Copy as an OSLC concept URL" and click "Copy OSLC URL". This URL looks like: <br>
 * <code>https://jazz-server/qm/oslc_qm/contexts/xxx/resources/com.ibm.rqm.planning.VersionedTestCase/_yyy?oslc_config.context=https://jazz-server/gc/configuration/1</code></li>
 * </ol>
 */
public class UpdateTestResultVerdict extends JazzSnippet {

    public static void main(String[] args) throws Exception {
        new UpdateTestResultVerdict().call();
    }

    @Override
    protected void run(OslcClient client) throws Exception {

        // load existing test result
        System.out.println("Reading test result...");
        URI resultUri = URI.create(getRequiredProperty("jazz.artifact"));
        ResourceResponse<TestResult> response = new GetResource<>(client, resultUri, TestResult.class).call();
        TestResult result = response.getEntity();
        String eTag = response.getETag();
        String shortIdentifier = ResourceProperties.getString(result, "http://jazz.net/ns/qm/rqm#shortIdentifier");
        System.out.println("Test Result: [" + shortIdentifier + "] " + result.getTitle());
        System.out.println("ETag: " + eTag);

        // load resource shape
        ResourceShape shape = new GetResourceShape(client, result.getInstanceShape()).get();
        Property verdictProperty = shape.getProperty(URI.create("http://jazz.net/ns/qm/rqm#verdict"));
        if (verdictProperty == null) {
            throw new RuntimeException("Verdict property is not defined for test result");
        }

        // determine possible verdicts
        List<String> verdictTitles = new ArrayList<>();
        Map<String, IExtendedResource> verdicts = new HashMap<>();
        for (Object verdict : verdictProperty.getAllowedValuesCollection()) {
            String title = ResourceProperties.getString((IExtendedResource) verdict, Dcterms.PROPERTY_TITLE);
            verdictTitles.add(title);
            verdicts.put(title, (IExtendedResource) verdict);
        }
        Collections.sort(verdictTitles);

        // print available verdicts
        System.out.println();
        System.out.println("Available verdicts:");
        for (int i = 0; i < verdictTitles.size(); i++) {
            System.out.println("[" + (i + 1) + "] " + verdictTitles.get(i));
        }

        // ask to pick one verdict
        System.out.println();
        System.out.println("Which verdict would you like to set? (from 1 to " + verdictTitles.size() + "):");
        System.out.print("> ");
        int verdictIndex;
        try (Scanner scanner = new Scanner(System.in, Charset.defaultCharset().name())) {
            verdictIndex = scanner.nextInt();
            if ((verdictIndex < 0) || (verdictIndex > (verdictTitles.size() - 1))) {
                throw new RuntimeException("Invalid verdict index");
            }
        }

        // change verdict in memory
        String newVerdictTitle = verdictTitles.get(verdictIndex - 1);
        URI newVerdict = verdicts.get(newVerdictTitle).getAbout();
        ResourceProperties.setURI(result, URI.create("http://jazz.net/ns/qm/rqm#verdict"), newVerdict);

        // ETM expects 'status' to be aligned with 'verdict'.
        // Status is the fragment of the verdict URI (e.g. com.ibm.rqm.execution.common.state.blocked)
        result.setStatus(newVerdict.getFragment());

        // update remote test result
        System.out.println("Updating result verdict with '" + newVerdictTitle + "'...");
        ResourceResponse<Void> updatedResponse = new PutResource<TestResult, Void>(client, resultUri, eTag, result).call();
        System.out.println("New ETag: " + updatedResponse.getETag());

        System.out.println("Done.");
    }

}
