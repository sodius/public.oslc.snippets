package com.sodius.oslc.snippets.app.jazz;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.eclipse.lyo.oslc4j.core.annotation.OslcName;
import org.eclipse.lyo.oslc4j.core.annotation.OslcNamespace;
import org.eclipse.lyo.oslc4j.core.annotation.OslcPropertyDefinition;
import org.eclipse.lyo.oslc4j.core.annotation.OslcResourceShape;
import org.eclipse.lyo.oslc4j.core.model.CreationFactory;
import org.eclipse.lyo.oslc4j.core.model.QueryCapability;
import org.eclipse.lyo.oslc4j.core.model.Service;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.eclipse.lyo.oslc4j.core.model.ServiceProviderCatalog;

import com.sodius.oslc.app.jazz.model.Friend;
import com.sodius.oslc.app.jazz.model.GlobalConfigurationAware;
import com.sodius.oslc.app.jazz.model.JazzNetProcess;
import com.sodius.oslc.app.jazz.requests.GetGlobalConfigurationFriend;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetResource;
import com.sodius.oslc.client.requests.GetResources;
import com.sodius.oslc.client.requests.GetRootServices;
import com.sodius.oslc.client.requests.GetServiceProvider;
import com.sodius.oslc.client.requests.GetServiceProviderCatalog;
import com.sodius.oslc.core.ldp.model.Ldp;
import com.sodius.oslc.core.model.OslcCore;
import com.sodius.oslc.core.model.ResourceProperties;
import com.sodius.oslc.core.model.RootServices;
import com.sodius.oslc.core.util.Uris;
import com.sodius.oslc.domain.config.model.Component;
import com.sodius.oslc.domain.config.model.Configuration;
import com.sodius.oslc.domain.config.model.OslcConfig;
import com.sodius.oslc.domain.config.requests.GetComponent;
import com.sodius.oslc.domain.config.requests.GetConfiguration;

/**
 * Prints out the local configurations of a DOORS Next project and all global configurations.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jazz.user</code> - user ID to connect to Jazz.</li>
 * <li><code>jazz.password</code> - password of the specified Jazz user.</li>
 * <li><code>jazz.project</code> - URL of the Jazz project, which can be obtained by executing ListProjectsSnippet.</li>
 * </ul>
 */
public class ListConfigurations extends JazzSnippet {

    public static void main(String[] args) throws Exception {
        new ListConfigurations().call();
    }

    @Override
    protected void run(OslcClient client) {

        // read provider
        System.out.println("Reading Project...");
        URI providerLocation = URI.create(getRequiredProperty("jazz.project"));
        ServiceProvider provider = new GetServiceProvider(client, providerLocation).get();

        // determine whether configuration management is enabled for this project
        boolean configurationEnabled = isConfigurationEnabled(provider);
        System.out.println("URL: <" + providerLocation + '>');
        System.out.println("Title: " + provider.getTitle());
        System.out.println("Configuration Enabled: " + configurationEnabled);
        System.out.println();

        if (configurationEnabled) {

            // load the root services
            RootServices rootServices = new GetRootServices<>(getRootServicesLocation(provider.getAbout()), RootServices.class).call();

            // global configurations
            new ListGlobalConfigurations(rootServices).run(client);

            System.out.println();

            // local configurations
            new ListLocalConfigurations(rootServices, provider).run(client);
        }
    }

    /*
     * Determine whether configuration management is enabled for this project
     */
    private static boolean isConfigurationEnabled(ServiceProvider provider) {
        return GlobalConfigurationAware.of(provider) == GlobalConfigurationAware.YES;
    }

    /*
     * Determine the URI of the root services given the URI of a resource within the application.
     */
    private static URI getRootServicesLocation(URI uri) {
        try {
            return new URI(Uris.getContext(uri) + "/rootservices");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private static void printConfiguration(Configuration configuration) {
        String type = "Configuration";
        if (configuration.getTypes().contains(URI.create(OslcConfig.TYPE_STREAM))) {
            type = "Stream";
        } else if (configuration.getTypes().contains(URI.create(OslcConfig.TYPE_BASELINE))) {
            type = "Baseline";
        }

        System.out.println("    [" + type + "] " + configuration.getTitle() + " <" + configuration.getAbout() + '>');
    }

    /*
     * Prints the components and configurations part of the specified Jazz project.
     */
    private static class ListLocalConfigurations extends JazzSnippet {

        private final RootServices rootServices;
        private final ServiceProvider provider;

        private ListLocalConfigurations(RootServices rootServices, ServiceProvider provider) {
            this.rootServices = rootServices;
            this.provider = provider;
        }

        @Override
        protected void run(OslcClient client) {
            System.out.println("Loading local configurations...");

            // load the configuration provider
            ServiceProvider configProvider = loadConfigProvider(client, rootServices);

            // loop on components
            for (Component component : loadComponents(client, configProvider)) {

                // list configurations of the component
                Collection<URI> configurationLocations = loadConfigurationLocations(client, component.getConfigurations());

                System.out.println("  [Component] " + component.getTitle() + " (" + configurationLocations.size() + " local configurations)");

                // loop on its configurations
                for (URI configurationLocation : configurationLocations) {
                    Configuration configuration = new GetConfiguration(client, configurationLocation).get();
                    printConfiguration(configuration);
                }
            }
        }

        private ServiceProvider loadConfigProvider(OslcClient client, RootServices rootServices) {

            // load the OSLC Config catalog
            ServiceProviderCatalog configCatalog = new GetServiceProviderCatalog(client, rootServices.getConfigCatalog()).get();

            // get the unique OSLC provider defined in there
            if (configCatalog.getServiceProviders().length != 1) {
                throw new RuntimeException("A unique service provider was expected in " + rootServices.getConfigCatalog());
            }

            // load the OSLC config provider
            return new GetServiceProvider(client, configCatalog.getServiceProviders()[0].getAbout()).get();
        }

        private Collection<Component> loadComponents(OslcClient client, ServiceProvider configProvider) {

            // lookup the component creation factory
            Optional<CreationFactory> uniqueFactory = Optional.empty();
            for (Service service : configProvider.getServices()) {
                CreationFactory factory = OslcCore.Finder.forCreationFactories(service).resourceType(URI.create(OslcConfig.TYPE_COMPONENT))
                        .findFirst();
                if (factory != null) {

                    // keep track of the first factory found
                    if (!uniqueFactory.isPresent()) {
                        uniqueFactory = Optional.of(factory);
                    }

                    // for RM, there's one factory per project (matching its title)
                    if (provider.getTitle().equals(factory.getTitle())) {

                        // Note: components are retrieved by sending a GET on the creation factory
                        return loadComponentDetails(client, factory.getCreation());
                    }
                }
            }

            // No direct match found, we're probably using a QM config provider.
            // In this case, there's only one factory available, which we can use to query the components
            // and retain the ones matching the expected service provider
            if (uniqueFactory.isPresent()) {

                // Note: components are retrieved by sending a GET on the creation factory
                return loadComponentMatchingProvider(client, uniqueFactory.get().getCreation());
            }

            throw new RuntimeException("A creation factory for components was expected in " + configProvider.getAbout());
        }

        private Collection<Component> loadComponentDetails(OslcClient client, URI uri) {
            Collection<Component> components = new GetResources<>(client, uri, Component.class).get();

            // load each Component to get the details
            Collection<Component> result = new ArrayList<>();
            for (Component component : components) {
                result.add(new GetComponent(client, component.getAbout()).get());
            }
            return result;
        }

        private Collection<Component> loadComponentMatchingProvider(OslcClient client, URI uri) {
            Collection<Component> components = new GetResources<>(client, uri, Component.class).get();

            // loop on components
            Collection<Component> result = new ArrayList<>();
            for (Component component : components) {

                // retain the ones whose project area matches the desired provider
                URI projectArea = ResourceProperties.getURI(component, JazzNetProcess.PROPERTY_PROJECT_AREA);
                if ((projectArea != null) && Arrays.asList(provider.getDetails()).contains(projectArea)) {
                    result.add(component);
                }
            }
            return result;
        }

        private Collection<URI> loadConfigurationLocations(OslcClient client, URI uri) {

            // get the URIs of the configurations
            Collection<URI> configurations = new GetResources<>(client, uri, URI.class).get();

            // No configuration at all? there should be at minimum one.
            // It's likely that the resource is a ldp:BasicContainer (case of QM)
            if (configurations.isEmpty()) {
                BasicContainer container = new GetResource<>(client, uri, BasicContainer.class).get();
                configurations = Arrays.asList(container.getContains());
            }

            return configurations;
        }
    }

    /*
     * Prints the GC projects and their configurations
     */
    private static class ListGlobalConfigurations extends JazzSnippet {

        private final RootServices rootServices;

        private ListGlobalConfigurations(RootServices rootServices) {
            this.rootServices = rootServices;
        }

        @Override
        protected void run(OslcClient client) {
            System.out.println("Loading global configurations...");

            // determine global configuration friend
            Optional<Friend> friend = new GetGlobalConfigurationFriend(client, rootServices.getAbout()).call();
            if (!friend.isPresent()) {
                System.out.println("The Jazz application has no GC friend application");
                return;
            }

            // load GC root services
            RootServices gcRootServices = new GetRootServices<>(friend.get().getRootServices(), RootServices.class).call();

            // load the OSLC Config catalog
            ServiceProviderCatalog configCatalog = new GetServiceProviderCatalog(client, gcRootServices.getConfigCatalog()).get();

            // loop on GC projects
            for (ServiceProvider provider : configCatalog.getServiceProviders()) {

                // load the full description of the provider if it doesn't contain services at this stage
                if (provider.getServices().length == 0) {
                    provider = new GetServiceProvider(client, provider.getAbout()).get();
                }

                System.out.println("  [Project] " + provider.getTitle());

                // determine the query to obtain configurations
                for (Service service : provider.getServices()) {
                    QueryCapability query = OslcCore.Finder.forQueryCapabilities(service).resourceType(URI.create(OslcConfig.TYPE_CONFIGURATION))
                            .findFirst();
                    if (query != null) {

                        // load configurations
                        Collection<Configuration> configurations = new GetResources<>(client, query.getQueryBase(), Configuration.class).get();
                        for (Configuration configuration : configurations) {
                            printConfiguration(configuration);
                        }
                    }
                }
            }
        }
    }

    @OslcName("BasicContainer")
    @OslcNamespace(Ldp.NAMESPACE_URI)
    @OslcResourceShape(title = "BasicContainer<URI> Resource Shape", describes = Ldp.TYPE_BASIC_CONTAINER)
    public static class BasicContainer {

        private final Set<URI> contains = new HashSet<>();

        @OslcName("contains")
        @OslcPropertyDefinition("http://www.w3.org/ns/ldp#contains")
        public URI[] getContains() {
            return contains.toArray(new URI[contains.size()]);
        }

        public void setContains(final URI[] contains) {
            this.contains.clear();

            if (contains != null) {
                this.contains.addAll(Arrays.asList(contains));
            }
        }
    }
}
