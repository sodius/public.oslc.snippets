package com.sodius.oslc.snippets.app.jazz.am;

import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.lyo.oslc4j.core.model.IExtendedResource;

import com.sodius.oslc.app.jazz.dm.model.JazzDm;
import com.sodius.oslc.client.ClientRuntimeException;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetResource;
import com.sodius.oslc.client.requests.GetResources;
import com.sodius.oslc.core.model.Dcterms;
import com.sodius.oslc.core.model.ResourceProperties;
import com.sodius.oslc.core.util.Uris;
import com.sodius.oslc.domain.am.model.ArchitectureResource;
import com.sodius.oslc.domain.config.ConfigContext;
import com.sodius.oslc.domain.config.model.OslcConfig;
import com.sodius.oslc.domain.rm.model.Requirement;
import com.sodius.oslc.snippets.app.jazz.JazzSnippet;

/**
 * Prints out the full hierarchy of RMM artifacts below a given resource.
 * Also prints out the links to requirements for each resource, if any.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jazz.user</code> - user ID to connect to Jazz.</li>
 * <li><code>jazz.password</code> - password of the specified Jazz user.</li>
 * <li><code>jazz.artifact</code> - URL of the root RMM artifact, which is the web URL in the browser when displaying the given artifact.</li>
 * </ul>
 */
public class ListArchitectureResources extends JazzSnippet {

    public static void main(String[] args) throws Exception {

        // There's no easy way in RMM to directly obtain the OSLC URL of a given architecture resource.
        // Therefore the input of this snippet is the web URL, as found in the browser location field,
        // that we convert to OSLC.

        // The web URL has this form:
        // https://myServer/ccm/web/projects/myProject#action=com.ibm.team.rmm.designs.viewResource&resourceUri=https://myServer/ccm/resource-rmm/1070436
        // &oslc_config.context=https://myServer/gc/configuration/1
        URI rmmWebArtifact = URI.create(getRequiredProperty("jazz.artifact"));

        // Determines the corresponding OSLC URL:
        // https://myServer/ccm/resource-rmm/1070436?oslc_config.context=https://myServer/gc/configuration/1
        URI resource = null;
        ConfigContext context = ConfigContext.empty();
        if (rmmWebArtifact.getFragment() != null) {
            String[] parameters = rmmWebArtifact.getFragment().split("&");
            for (String parameter : parameters) {
                String[] parameterParts = parameter.split("=");
                if (parameterParts.length == 2) {
                    String parameterName = parameterParts[0];
                    String parameterValue = parameterParts[1];

                    if ("resourceUri".equals(parameterName)) {
                        resource = Uris.create(parameterValue);
                    } else if (OslcConfig.CONFIG_CONTEXT_PARAMETER.equals(parameterName)) {
                        context = ConfigContext.of(Uris.create(parameterValue));
                    }
                }
            }
        }
        if (resource == null) {
            throw new IllegalArgumentException("This web URL cannot be converted to an OSLC URL: " + rmmWebArtifact);
        }

        new ListArchitectureResources(resource, context).call();
    }

    private final URI rootArtifactLocation;
    private final ConfigContext context;
    private final Map<URI, Requirement> requirements;

    private ListArchitectureResources(URI rootArtifactLocation, ConfigContext context) {
        this.rootArtifactLocation = rootArtifactLocation;
        this.context = context;
        this.requirements = new HashMap<>();
    }

    @Override
    protected void run(OslcClient client) {
        printResource(client, rootArtifactLocation, "");
    }

    private void printResource(OslcClient client, URI artifactLocation, String printIndent) {

        // load the artifact
        ArchitectureResource resource = loadResource(client, artifactLocation);
        if (resource == null) {
            return;
        }

        // Determines its type
        Object type = ResourceProperties.getObject(resource, "http://jazz.net/ns/am/rmm#type");
        String typeName = "unknown";
        if (type instanceof IExtendedResource) {
            typeName = ResourceProperties.getString((IExtendedResource) type, Dcterms.PROPERTY_TITLE);
        }

        // print this artifact
        System.out.println(printIndent + "[" + typeName + "] " + resource.getTitle());

        // print linked requirements
        printLinkedRequirements(client, resource, URI.create(JazzDm.PROPERTY_DERIVES), "derives", printIndent);
        printLinkedRequirements(client, resource, URI.create(JazzDm.PROPERTY_REFINE), "refines", printIndent);
        printLinkedRequirements(client, resource, URI.create(JazzDm.PROPERTY_SATISFY), "satisfies", printIndent);
        printLinkedRequirements(client, resource, URI.create(JazzDm.PROPERTY_TRACE), "traces", printIndent);

        // recurse on aggregated resources
        printChildResources(client, resource, printIndent);
    }

    private void printLinkedRequirements(OslcClient client, ArchitectureResource resource, URI linkType, String linkTypeName, String printIndent) {
        Collection<URI> requirementLocations = ResourceProperties.getURIs(resource, linkType);
        if (!requirementLocations.isEmpty()) {
            System.out.println(printIndent + "  " + linkTypeName + " (" + requirementLocations.size() + "):");
            for (URI requirementLocation : requirementLocations) {
                Requirement requirement = loadRequirement(client, requirementLocation);
                System.out.println(printIndent + "  - " + requirement.getIdentifier() + ": " + requirement.getTitle());
            }
        }
    }

    private void printChildResources(OslcClient client, ArchitectureResource resource, String printIndent) {
        Collection<URI> childrenLocations = ResourceProperties.getURIs(resource, "http://jazz.net/ns/am/rmm#aggregate");
        if (!childrenLocations.isEmpty()) {
            System.out.println(printIndent + "  aggregates (" + childrenLocations.size() + "):");
            for (URI childLocation : childrenLocations) {
                printResource(client, childLocation, printIndent + "    ");
            }
        }
    }

    /*
     * Loads the given architecture resource
     */
    private ArchitectureResource loadResource(OslcClient client, URI artifactLocation) {
        try {
            // A RMM response may contain multiple architecture resources.
            // Need to get all of them and retain the one matching the expected URI
            URI versionedUri = context.apply(artifactLocation);
            Collection<ArchitectureResource> resources = new GetResources<>(client, versionedUri, ArchitectureResource.class).get();
            for (ArchitectureResource resource : resources) {
                if (artifactLocation.equals(resource.getAbout())) {
                    return resource;
                }
            }

            return null;
        } catch (ClientRuntimeException e) {
            System.err.println("Could not load " + artifactLocation + ": " + e);
            return null;
        }
    }

    /*
     * Loads the given requirement.
     * Uses a cache, not to load twice the same requirement, that could be a target of multiple links.
     */
    private Requirement loadRequirement(OslcClient client, URI requirementLocation) {
        return this.requirements.computeIfAbsent(requirementLocation, uri -> {
            URI versionedUri = context.apply(uri);
            return new GetResource<>(client, versionedUri, Requirement.class).get();
        });
    }

}
