package com.sodius.oslc.snippets.app.jazz.qm;

import java.net.URI;
import java.util.Optional;

import javax.ws.rs.core.HttpHeaders;

import org.eclipse.lyo.oslc4j.core.model.CreationFactory;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.Service;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;

import com.sodius.oslc.app.jazz.model.GlobalConfigurationAware;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetServiceProvider;
import com.sodius.oslc.client.requests.PostResource;
import com.sodius.oslc.client.requests.ResourceResponse;
import com.sodius.oslc.core.model.OslcCore;
import com.sodius.oslc.domain.qm.model.OslcQm;
import com.sodius.oslc.domain.qm.model.TestCase;
import com.sodius.oslc.snippets.app.jazz.JazzSnippet;

/**
 * Creates a new test case in a given project.
 * </p>
 *
 * <p>
 * Required Program arguments:
 * </p>
 * <ul>
 * <li><code>args[0]</code>: title to assign to the new ETM test case</li>
 * <li><code>args[1]</code> (optional): description to assign to the ETM test case</li>
 * <li><code>args[2]</code> (optional): URI of a requirement that the test case validates</li>
 * </ul>
 * <p>
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jazz.user</code> - user ID to connect to Jazz.</li>
 * <li><code>jazz.password</code> - password of the specified Jazz user.</li>
 * <li><code>jazz.project</code> - URL of the Jazz project, which can be obtained by executing ListProjectsSnippet.</li>
 * </ul>
 */
public class CreateTestCase extends JazzSnippet {

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            throw new IllegalArgumentException("Expects a requirement title as command line argument");
        }
        String title = args[0];
        Optional<String> description = args.length > 1 ? Optional.of(args[1]) : Optional.empty();
        Optional<URI> validatedRequirement = args.length > 2 ? Optional.of(URI.create(args[2])) : Optional.empty();
        new CreateTestCase(title, description, validatedRequirement).call();
    }

    private final String title;
    private final Optional<String> description;
    private final Optional<URI> validatedRequirement;

    private CreateTestCase(String title, Optional<String> description, Optional<URI> validatedRequirement) {
        this.title = title;
        this.description = description;
        this.validatedRequirement = validatedRequirement;
    }

    @Override
    protected void run(OslcClient client) throws Exception {

        // read service provider
        System.out.println("Reading Project...");
        URI providerLocation = URI.create(getRequiredProperty("jazz.project"));
        ServiceProvider provider = new GetServiceProvider(client, providerLocation).get();

        // print its properties
        System.out.println("URL: <" + providerLocation + '>');
        System.out.println("Title: " + provider.getTitle());
        System.out.println("Global Configuration Aware: " + GlobalConfigurationAware.of(provider));

        // lookup the test case creation factory
        Service service = OslcCore.Finder.forServices(provider).domain(URI.create(OslcQm.DOMAIN)).findFirst();
        CreationFactory factory = OslcCore.Finder.forCreationFactories(service).resourceType(URI.create(OslcQm.TYPE_TEST_CASE)).findFirst();
        if (factory == null) {
            throw new RuntimeException("No creation factory available for creating a test case");
        }

        // create a test case in memory
        TestCase testCase = new TestCase();
        testCase.setTitle(title);
        if (description.isPresent()) {
            testCase.setDescription(description.get());
        }
        if (validatedRequirement.isPresent()) {
            testCase.addValidatesRequirement(new Link(validatedRequirement.get()));
        }

        // create the remote test case
        System.out.println("Creating the test case...");
        ResourceResponse<Void> createResponse = new PostResource<TestCase, Void>(client, factory.getCreation(), testCase).call();
        System.out.println("Created test case: " + createResponse.getHeaders().getFirst(HttpHeaders.LOCATION));
        System.out.println("ETag: " + createResponse.getETag());

        System.out.println("Done.");
    }

}
