package com.sodius.oslc.snippets.app.jazz;

import java.net.URI;

import org.eclipse.lyo.oslc4j.core.model.Link;

import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.core.process.links.model.DirectedLink;
import com.sodius.oslc.core.process.links.requests.RemoveLink;
import com.sodius.oslc.core.process.model.LinkType;
import com.sodius.oslc.domain.config.model.OslcConfig;
import com.sodius.oslc.snippets.utils.LinkTypeUtils;

/**
 * Removes a link between source and target artifacts in Jazz applications.
 *
 * <p>
 * The Jazz artifact are either:
 * <ul>
 * <li>a Change Request in /ccm</li>
 * <li>a Requirement in /rm</li>
 * <li>a Test artifact in /qm (Test Plan, Test Case, Test Execution Record, Test Case Result or Test Script)</li>
 * </ul>
 *
 * <h3>Arguments</h3>
 * <p>
 * This snippet expects the following ordered program arguments:
 * <ol>
 * <li>The URL of a Jazz source artifact</li>
 * <li>The link type to create (e.g. <code>IMPLEMENTS_REQUIREMENT</code>)</li>
 * <li>The URL of a Jazz target artifact</li>
 * </ol>
 *
 * <p>
 * This snippet expects the following Java virtual machine arguments:
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jazz.user</code> - user ID to connect to Jazz.</li>
 * <li><code>jazz.password</code> - password of the specified Jazz user.</li>
 * </ul>
 *
 * <p>
 * Java virtual machine arguments are set in the command line with:<br>
 * <code>-Dkey=value</code><br>
 * e.g.<br>
 * <code>-Djazz.user=patricia</code>
 *
 * <h3>Link Type</h3>
 * Here are the supported link types:
 * <table border="1">
 * <tr>
 * <th>Source</th>
 * <th>Link Type</th>
 * <th>Target</th>
 * </tr>
 *
 * <tr>
 * <td>Change Request</td>
 * <td>
 * <ul>
 * <li>"Provides - Related Change Requests" association
 * <ul>
 * <li><code>AFFECTS_PLAN_ITEM</code></li>
 * <li><code>RELATED_CHANGE_REQUEST</code></li>
 * </ul>
 * </td>
 * <td>Change Request</td>
 * </tr>
 *
 * <tr>
 * <td>Change Request</td>
 * <td>
 * <ul>
 * <li>"Provides - Implementation Requests" association
 * <ul>
 * <li><code>AFFECTS_REQUIREMENT</code></li>
 * <li><code>IMPLEMENTS_REQUIREMENT</code></li>
 * </ul>
 * </li>
 * <li>"Provides - Requirements Change Requests" association
 * <ul>
 * <li><code>TRACKS_REQUIREMENT</code></li>
 * </ul>
 * </td>
 * <td>Requirement</td>
 * </tr>
 *
 * <tr>
 * <td>Change Request</td>
 * <td>
 * <ul>
 * <li>"Provides - Defects" association
 * <ul>
 * <li><code>AFFECTS_TEST_RESULT</code></li>
 * <li><code>BLOCKS_TEST_EXECUTION_RECORD</code></li>
 * <li><code>TESTED_BY_TEST_CASE</code></li>
 * </ul>
 * </li>
 * <li>"Provides - Quality Management Tasks" association
 * <ul>
 * <li><code>RELATED_TEST_PLAN</code></li>
 * <li><code>RELATED_TEST_CASE</code></li>
 * <li><code>RELATED_TEST_EXECUTION_RECORD</code></li>
 * <li><code>RELATED_TEST_SCRIPT</code></li>
 * </ul>
 * </li>
 * </ul>
 * </td>
 * <td>Test Artifact</td>
 * </tr>
 *
 * </table>
 *
 * <h3>Jazz Artifact URL</h3>
 * <p>
 * The URL of the Jazz artifact is the OSLC URL of the artifact, which you can obtain from the web page of the artifact:
 * <ul>
 * <li>In /ccm:
 * <ol>
 * <li>Navigate to the work item page.</li>
 * <li>Click the "Copy ID and Summary" button in the tool bar</li>
 * <li>Copy the URL of the hyperlink displayed in the dialog. This URL looks like: <br>
 * <code>https://jazz-server/ccm/resource/itemName/com.ibm.team.workitem.WorkItem/1</code></li>
 * </ol>
 * </li>
 * <li>In /rm:
 * <ol>
 * <li>Navigate to the requirement page.</li>
 * <li>Click the "More actions" button in the tool bar and click "Share Link to Artifact..."</li>
 * <li>Copy the URL displayed in the dialog. This URL looks like: <br>
 * <code>https://jazz-server/rm/resources/xxx?oslc_config.context=https://jazz-server/gc/configuration/1</code></li>
 * </ol>
 * </li>
 * <li>In /qm (versions 6.0.6.1 and higher):
 * <ol>
 * <li>Navigate to the test artifact page.</li>
 * <li>Click the "Copy link for this page" button in the tool bar</li>
 * <li>Click "Copy as an OSLC concept URL" and click "Copy OSLC URL". This URL looks like: <br>
 * <code>https://jazz-server/qm/oslc_qm/contexts/xxx/resources/com.ibm.rqm.planning.VersionedTestCase/_yyy?oslc_config.context=https://jazz-server/gc/configuration/1</code></li>
 * </ol>
 * </li>
 * </ul>
 */
public class RemoveJazzLink extends JazzSnippet {

    /*
     * See the class documentation for details on expected arguments
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 3) {
            throw new IllegalArgumentException("Expecting 3 program arguments");
        }

        // validate the Jazz source artifact URL
        URI sourceLocation = URI.create(args[0]);

        // validate the link type
        LinkType linkType = LinkType.valueOf(args[1]);

        // validate the Jazz target artifact URL
        URI targetLocation = URI.create(args[2]);

        // run the snippet
        new RemoveJazzLink(sourceLocation, linkType, targetLocation).call();
    }

    private final URI sourceArtifactLocation;
    private final LinkType linkType;
    private final URI targetArtifactLocation;

    private RemoveJazzLink(URI sourceArtifactLocation, LinkType linkType, URI targetArtifactLocation) {
        this.sourceArtifactLocation = sourceArtifactLocation;
        this.linkType = linkType;
        this.targetArtifactLocation = targetArtifactLocation;
    }

    @Override
    protected void run(OslcClient client) throws Exception {
        removeLinkOnSource(client);
        removeLinkOnTarget(client);
        System.out.println("Done.");
    }

    private void removeLinkOnSource(OslcClient client) {
        System.out.println("Removing '" + LinkTypeUtils.getTitle(linkType) + "' link on source artifact...");
        URI unversionedTarget = OslcConfig.removeContext(targetArtifactLocation);
        removeLink(client, sourceArtifactLocation, linkType, unversionedTarget);
    }

    private void removeLinkOnTarget(OslcClient client) {

        // Ensure there is a back-link to create.
        if (linkType.getBacklink().isPresent()) {
            LinkType backLinkType = linkType.getBacklink().get();
            System.out.println("Removing '" + LinkTypeUtils.getTitle(backLinkType) + "' back-link on target artifact...");

            // Note that if the Jazz project is global configuration enabled, there won't be any back link to create,
            // as incoming links will be discovered by Jazz.
            // The AddLink class will take care of adding the link only if necessary.
            URI unversionedSource = OslcConfig.removeContext(sourceArtifactLocation);
            removeLink(client, targetArtifactLocation, backLinkType, unversionedSource);
        }

        else {
            System.out.println("No back-link is to remove on target artifact");
        }
    }

    private void removeLink(OslcClient client, URI source, LinkType linkType, URI target) {
        DirectedLink link = new DirectedLink(linkType.getPropertyDefinition(), source, new Link(target));
        new RemoveLink(client, link).call();
    }
}
