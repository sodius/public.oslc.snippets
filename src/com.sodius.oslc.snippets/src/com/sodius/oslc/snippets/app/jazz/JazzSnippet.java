package com.sodius.oslc.snippets.app.jazz;

import java.util.concurrent.Callable;

import org.apache.http.auth.UsernamePasswordCredentials;

import com.sodius.oslc.client.ClientWebException;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.OslcClients;

/**
 * Base class for Jazz snippets.
 *
 * <p>
 * This snippet expects the following Java virtual machine arguments:
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jazz.user</code> - user ID to connect to Jazz.</li>
 * <li><code>jazz.password</code> - password of the specified Jazz user.</li>
 * </ul>
 */
public abstract class JazzSnippet implements Callable<Void> {

    /**
     * Creates an OSLC client to connect to Jazz and calls the <code>run()</code> method.
     *
     * @see #run(OslcClient)
     */
    @Override
    public final Void call() throws Exception {
        try {
            OslcClient client = createJazzClient();
            run(client);
        } catch (ClientWebException e) {
            System.err.println("Failed to execute " + e.getRequest().getURI());
            System.err.println("Status " + e.getResponse().getStatusCode() + " - " + e.getResponse().getMessage());
            System.err.println("Entity: " + e.getResponse().getEntity(String.class));
        }

        return null;
    }

    /**
     * Sub-classes must implement what's to do when an OSLC client is available for Jazz.
     */
    protected abstract void run(OslcClient client) throws Exception;

    /**
     * Creates an OSLC client to connect to a Jazz environment.
     * The client uses FORM based authentication, which is made when the first request is executed on the server.
     * Client uses credentials specified with <code>jazz.user</code> and <code>jazz.password</code> Java virtual machine arguments.
     */
    private static OslcClient createJazzClient() {
        String userName = getRequiredProperty("jazz.user");
        String password = getRequiredProperty("jazz.password");
        return OslcClients.jazzForm(new UsernamePasswordCredentials(userName, password)).create();
    }

    protected static String getRequiredProperty(String name) {
        String value = System.getProperty(name);
        if ((value == null) || value.isEmpty()) {
            throw new IllegalArgumentException("Missing required Java virtual machine argument: " + name);
        } else {
            return value;
        }
    }
}
