package com.sodius.oslc.snippets.app.jazz.qm;

import java.net.URI;
import java.util.Optional;

import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetResource;
import com.sodius.oslc.client.requests.PutResource;
import com.sodius.oslc.client.requests.ResourceResponse;
import com.sodius.oslc.core.model.ResourceProperties;
import com.sodius.oslc.domain.qm.model.TestCase;
import com.sodius.oslc.snippets.app.jazz.JazzSnippet;

/**
 * Updates the title and description of the specified test case.
 * </p>
 *
 * <p>
 * Required Program arguments:
 * </p>
 * <ul>
 * <li><code>args[0]</code>: new title to assign to the ETM test case</li>
 * <li><code>args[1]</code> (optional): new description to assign to the ETM test case</li>
 * </ul>
 * <p>
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jazz.user</code> - user ID to connect to Jazz.</li>
 * <li><code>jazz.password</code> - password of the specified Jazz user.</li>
 * <li><code>jazz.artifact</code> - URL of the ETM test case.</li>
 * </ul>
 *
 * <h3>Test Case URL</h3>
 * <p>
 * The URL of the test case is the OSLC URL of the artifact, which you can obtain from /qm (versions 6.0.6.1 and higher):
 * <ol>
 * <li>Navigate to the test artifact page.</li>
 * <li>Click the "Copy link for this page" button in the tool bar</li>
 * <li>Click "Copy as an OSLC concept URL" and click "Copy OSLC URL". This URL looks like: <br>
 * <code>https://jazz-server/qm/oslc_qm/contexts/xxx/resources/com.ibm.rqm.planning.VersionedTestCase/_yyy?oslc_config.context=https://jazz-server/gc/configuration/1</code></li>
 * </ol>
 */
public class UpdateTestCase extends JazzSnippet {

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            throw new IllegalArgumentException("Expects a requirement title as command line argument");
        }
        String title = args[0];
        Optional<String> description = args.length > 1 ? Optional.of(args[1]) : Optional.empty();
        new UpdateTestCase(title, description).call();
    }

    private final String title;
    private final Optional<String> description;

    private UpdateTestCase(String title, Optional<String> description) {
        this.title = title;
        this.description = description;
    }

    @Override
    protected void run(OslcClient client) throws Exception {

        // load existing test case
        System.out.println("Reading test case...");
        URI testCaseUri = URI.create(getRequiredProperty("jazz.artifact"));
        ResourceResponse<TestCase> response = new GetResource<>(client, testCaseUri, TestCase.class).call();
        TestCase testCase = response.getEntity();
        String eTag = response.getETag();
        String shortIdentifier = ResourceProperties.getString(testCase, "http://jazz.net/ns/qm/rqm#shortIdentifier");
        System.out.println("Test Case: [" + shortIdentifier + "] " + testCase.getTitle());
        System.out.println("ETag: " + eTag);

        // change title in memory
        testCase.setTitle(title);
        if (description.isPresent()) {
            testCase.setDescription(description.get());
        }

        // update remote test case
        System.out.println("Updating test case title...");
        ResourceResponse<Void> updatedResponse = new PutResource<TestCase, Void>(client, testCaseUri, eTag, testCase).call();
        System.out.println("New ETag: " + updatedResponse.getETag());

        System.out.println("Done.");
    }

}
