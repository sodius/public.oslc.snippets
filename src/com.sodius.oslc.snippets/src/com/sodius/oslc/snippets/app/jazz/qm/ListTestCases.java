package com.sodius.oslc.snippets.app.jazz.qm;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.lyo.oslc4j.core.model.QueryCapability;
import org.eclipse.lyo.oslc4j.core.model.Service;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;

import com.sodius.oslc.app.jazz.model.GlobalConfigurationAware;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetResources;
import com.sodius.oslc.client.requests.GetServiceProvider;
import com.sodius.oslc.core.model.OslcCore;
import com.sodius.oslc.core.model.ResourceProperties;
import com.sodius.oslc.core.util.Uris;
import com.sodius.oslc.domain.config.model.OslcConfig;
import com.sodius.oslc.domain.qm.model.OslcQm;
import com.sodius.oslc.domain.qm.model.TestCase;
import com.sodius.oslc.snippets.app.jazz.JazzSnippet;
import com.sodius.oslc.snippets.utils.TitleComparator;

/**
 * Queries and prints out the test cases in a given project.
 * </p>
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jazz.user</code> - user ID to connect to Jazz.</li>
 * <li><code>jazz.password</code> - password of the specified Jazz user.</li>
 * <li><code>jazz.project</code> - URL of the Jazz project, which can be obtained by executing ListProjectsSnippet.</li>
 * </ul>
 */
public class ListTestCases extends JazzSnippet {

    public static void main(String[] args) throws Exception {
        new ListTestCases().call();
    }

    private ListTestCases() {
    }

    @Override
    protected void run(OslcClient client) throws Exception {

        // read service provider
        System.out.println("Reading Project...");
        URI providerLocation = URI.create(getRequiredProperty("jazz.project"));
        ServiceProvider provider = new GetServiceProvider(client, providerLocation).get();

        // print its properties
        System.out.println("URL: <" + providerLocation + '>');
        System.out.println("Title: " + provider.getTitle());
        System.out.println("Global Configuration Aware: " + GlobalConfigurationAware.of(provider));

        // lookup capability to query test cases
        Service service = OslcCore.Finder.forServices(provider).domain(URI.create(OslcQm.DOMAIN)).findFirst();
        QueryCapability queryCapability = OslcCore.Finder.forQueryCapabilities(service)
                .resourceType(URI.create("http://open-services.net/ns/qm#TestCaseQuery")).findFirst();

        // run the query
        System.out.println();
        System.out.println("Querying for test cases...");
        URI query = getQuery(queryCapability);
        URI versionedQuery = OslcConfig.addContext(query, OslcConfig.getContext(providerLocation));
        Collection<TestCase> testCases = new GetResources<>(client, versionedQuery, TestCase.class).get();

        // sort requirements by title
        List<TestCase> sortedTestCases = new ArrayList<>(testCases);
        Collections.sort(sortedTestCases, new TitleComparator());

        // print the test cases
        System.out.println();
        System.out.println("Test Cases (" + sortedTestCases.size() + ": ");
        for (TestCase testCase : sortedTestCases) {
            String shortIdentifier = ResourceProperties.getString(testCase, "http://jazz.net/ns/qm/rqm#shortIdentifier");
            System.out.println("- [" + shortIdentifier + "] " + testCase.getTitle() + " <" + testCase.getAbout() + '>');
        }

        System.out.println("Done.");
    }

    /*
     * Determines the actual query to perform on test cases.
     */
    private URI getQuery(QueryCapability query) throws UnsupportedEncodingException {
        String prefixes = "dcterms=<http://purl.org/dc/terms/>,rqm_qm=<http://jazz.net/ns/qm/rqm#shortIdentifier>";
        String select = "dcterms:title,rqm_qm:shortIdentifier";

        // @formatter:off
        return Uris.builder(query.getQueryBase())
            .queryParam("oslc.prefix", URLEncoder.encode(prefixes, StandardCharsets.UTF_8.name()))
            .queryParam("oslc.select", select)
            .build();
        // @formatter:on
    }

}
