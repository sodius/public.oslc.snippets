package com.sodius.oslc.snippets.app.jazz;

import java.net.URI;

import org.eclipse.lyo.oslc4j.core.model.CreationFactory;
import org.eclipse.lyo.oslc4j.core.model.Dialog;
import org.eclipse.lyo.oslc4j.core.model.QueryCapability;
import org.eclipse.lyo.oslc4j.core.model.Service;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;

import com.sodius.oslc.app.jazz.model.GlobalConfigurationAware;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetServiceProvider;
import com.sodius.oslc.core.model.OslcCore;

/**
 * Reads the OSLC information of a Jazz project.
 *
 * <p>
 * System properties are configured in an Eclipse Java launch configuration in the "VM arguments" area of the "Arguments" tab.
 * </p>
 *
 * <p>
 * Here are the required System properties to execute the snippet:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: location of the license file in your file system. Contact us to obtain one.</li>
 * <li><code>jazz.user</code> - user ID to connect to Jazz.</li>
 * <li><code>jazz.password</code> - password of the specified Jazz user.</li>
 * <li><code>jazz.project</code> - URL of the Jazz project, which can be obtained by executing ListProjectsSnippet.</li>
 * </ul>
 */
public class ReadProjectSnippet extends JazzSnippet {

    public static void main(String[] args) throws Exception {
        new ReadProjectSnippet().call();
    }

    @Override
    protected void run(OslcClient client) {

        // read service provider
        System.out.println("Reading Project...");
        URI providerLocation = URI.create(getRequiredProperty("jazz.project"));
        ServiceProvider provider = new GetServiceProvider(client, providerLocation).get();

        // print its properties
        System.out.println("URL: <" + providerLocation + '>');
        System.out.println("Title: " + provider.getTitle());
        System.out.println("Description: " + provider.getDescription());
        System.out.println("Global Configuration Aware: " + GlobalConfigurationAware.of(provider));

        // print its services
        System.out.println("\nServices:\n");
        for (Service service : provider.getServices()) {
            System.out.println("Domain: " + service.getDomain());

            CreationFactory creationFactory = OslcCore.Finder.forCreationFactories(service).defaultUsage().findFirst();
            System.out.println("- Default creation factory: " + (creationFactory == null ? "none" : creationFactory.getTitle()));

            QueryCapability queryCapability = OslcCore.Finder.forQueryCapabilities(service).defaultUsage().findFirst();
            System.out.println("- Default query capability: " + (queryCapability == null ? "none" : queryCapability.getTitle()));

            Dialog selectionDialog = OslcCore.Finder.forSelectionDialogs(service).defaultUsage().findFirst();
            System.out.println("- Default selection dialog: " + (selectionDialog == null ? "none" : selectionDialog.getLabel()));

            Dialog creationDialog = OslcCore.Finder.forCreationDialogs(service).defaultUsage().findFirst();
            System.out.println("- Default creation dialog: " + (creationDialog == null ? "none" : creationDialog.getLabel()));

            System.out.println();
        }
    }
}
