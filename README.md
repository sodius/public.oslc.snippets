# MDAccess for OSLC Snippets

This repository holds resources for a Java developer to act as a client of OSLC applications, 
to query resources and make updates in an OSLC-enabled application.

See [https://www.sodiuswillert.com](https://www.sodiuswillert.com)

### Preparing your Eclipse Platform

Eclipse is the recommended platform to execute the snippets.

Here are the necessary steps to have a working environment for the snippets: 

1. In Eclipse, import the [com.sodius.oslc.snippets](https://bitbucket.org/sodius/public.oslc.snippets/src/master/src/com.sodius.oslc.snippets) project from the snippets repository. It is expected the code doesn't yet compile as the necessary libraries are not yet installed.
2. Select the **/com.sodius.oslc.snippets/target/snippets.target** file and right-click **Open With > Target Editor**
3. Click **Set as Active Platform** on top right

At this stage the code should successfully compile in **com.sodius.oslc.snippets** project.

### Obtaining a License

MDAccess for OSLC is license protected. Contact us to get a SodiusWillert license file to run the snippets

### Executing Snippets

This repository contains snippets demonstrating how to use an OSLC client to query and update resources in an OSLC application
and to create links between OSLC resources.

Those snippets are located here: [/src/com.sodius.oslc.snippets/src/com/sodius/oslc/snippets/app/jazz](https://bitbucket.org/sodius/public.oslc.snippets/src/master/src/com.sodius.oslc.snippets/src/com/sodius/oslc/snippets/app/jazz)

The Javadoc of each snippet details the expected program arguments and Java VM arguments for the snippet to run properly.

The snippets use MDAccess for OSLC to connect to OSLC-enabled applications. 
You may refer to its [Developer Guide](https://help.sodius.cloud/help/topic/com.sodius.oslc.doc/html/overview.html) for more details.

### Snippet List

Here are the snippets you may want to start with:

* [ReadApplicationVersion](https://bitbucket.org/sodius/public.oslc.snippets/src/master/src/com.sodius.oslc.snippets/src/com/sodius/oslc/snippets/app/jazz/ReadApplicationVersion.java): read version for the given application
* [ListProjectsSnippet](https://bitbucket.org/sodius/public.oslc.snippets/src/master/src/com.sodius.oslc.snippets/src/com/sodius/oslc/snippets/app/jazz/ListProjectsSnippet.java): list available projects in a Jazz application
* [ListConfigurations](https://bitbucket.org/sodius/public.oslc.snippets/src/master/src/com.sodius.oslc.snippets/src/com/sodius/oslc/snippets/app/jazz/ListConfigurations.java): list local configurations of a Jazz application and GCM global configurations

Snippets for creating a removing links between two OSLC resources:

* [CreateJazzLink](https://bitbucket.org/sodius/public.oslc.snippets/src/master/src/com.sodius.oslc.snippets/src/com/sodius/oslc/snippets/app/jazz/CreateJazzLink.java): creates a link between Jazz source and target artifacts
* [RemoveJazzLink](https://bitbucket.org/sodius/public.oslc.snippets/src/master/src/com.sodius.oslc.snippets/src/com/sodius/oslc/snippets/app/jazz/RemoveJazzLink.java): removes a link between Jazz source and target artifacts

Snippets for updating ETM test artifacts:

* [ListTestCases](https://bitbucket.org/sodius/public.oslc.snippets/src/master/src/com.sodius.oslc.snippets/src/com/sodius/oslc/snippets/app/jazz/qm/ListTestCases.java): queries the test cases in a given project
* [CreateTestCase](https://bitbucket.org/sodius/public.oslc.snippets/src/master/src/com.sodius.oslc.snippets/src/com/sodius/oslc/snippets/app/jazz/qm/CreateTestCase.java): creates a test case with a given title, description and link to a requirement
* [UpdateTestCase](https://bitbucket.org/sodius/public.oslc.snippets/src/master/src/com.sodius.oslc.snippets/src/com/sodius/oslc/snippets/app/jazz/qm/UpdateTestCase.java): updates the title and description of a test case
* [UpdateTestResultVerdict](https://bitbucket.org/sodius/public.oslc.snippets/src/master/src/com.sodius.oslc.snippets/src/com/sodius/oslc/snippets/app/jazz/qm/UpdateTestResultVerdict.java): updates the verdict and status of a test case result

Snippets for reading RMM architecture resources:

* [ListArchitectureResources](https://bitbucket.org/sodius/public.oslc.snippets/src/master/src/com.sodius.oslc.snippets/src/com/sodius/oslc/snippets/app/jazz/am/ListArchitectureResources.java): explore a hierarchy of architecture resources and prints linked requirements


The snippets package contains more snippets, make sure to review all of them to see all capabilities being demonstrated.
